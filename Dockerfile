### Build stage
FROM python:3.12.4-slim AS builder

LABEL maintainer="Gabriel Acosta"

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PIP_NO_CACHE_DIR=1

ENV WORKDIR /app

WORKDIR ${WORKDIR}

COPY requirements/ /tmp

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r /tmp/base.txt -r /tmp/dev.txt

### App stage
FROM python:3.12.4-slim

RUN useradd --create-home app

ENV HOME=/home/app
ENV APP_HOME=$HOME/web
ENV PATH "${HOME}/.local/bin:${PATH}"
RUN mkdir $APP_HOME
# RUN mkdir $APP_HOME/staticfiles
# RUN mkdir -p $APP_HOME/media/uploads

# Install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
  netcat-traditional \
  postgresql-client \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR $APP_HOME

COPY entrypoint.sh .
RUN chmod +x $APP_HOME/entrypoint.sh

COPY --from=builder /app/wheels /wheels

COPY . $APP_HOME

RUN chown -R app:app $APP_HOME

USER app

RUN pip install --no-cache --disable-pip-version-check /wheels/*

ENTRYPOINT [ "/home/app/web/entrypoint.sh" ]
