MANAGE := docker-compose exec backend python manage.py
COMPOSE_FILE_DEFAULT_NAME := docker-compose.yml

PWD := $(dir $(ME))

file ?= $(PWD)$(COMPOSE_FILE_DEFAULT_NAME)

help:
	@echo "build"
	@echo "down"
	@echo "logs"
	@echo "stop"
	@echo "bash <serivce>"
	@echo "tests"
	@echo "test_<test_name>"

build:
	@docker-compose -f "$(file)" build 
	@docker pull postgres:16-alpine

up:
	@docker-compose -f "$(file)" up --detach --remove-orphans --force-recreate

down:
	@docker-compose -f "$(file)" down

logs:
	@docker-compose -f "$(file)" logs --follow

stop:
	@docker-compose -f "$(file)" stop

bash:
	@echo
	@echo "Starting shell in container '$(container)'"
	@docker-compose exec "$(container)" bash

makemigrations:
	@$(MANAGE) makemigrations

migrate:
	@$(MANAGE) migrate

createadmin:
	@$(MANAGE) createsuperuser

tests:
	@docker-compose -f "$(file)" exec backend pytest --cov=tudu --cov-report=term-missing tests/unit

test_%:
	@docker-compose -f "$(file)" exec backend pytest -s tests/*/$@.py


.PHONY: build up down logs stop bash makemigrations migrate createadmin tests test_
