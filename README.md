# Tudu
> [!NOTE]  
> El sistema en el que se desarrolló y probó todo tiene lo siguiente.
> - OS: Arch Linux
> - Docker: 26.1.4, build 5650f9b102
> - Docker compose: version 2.28.1
> - Python: 3.12.4

## Running backend
Ejecutar un `make up`.
 
> [!TIP]
> `make` para ver más atajos.

Configurar la DB:
```
make migrate
make createadmin
```

Para ver los logs:
```
make logs
```

## Running frontend
El frontend es una aplicación CLI muy básica. No está containerizada, asi que se puede ejecutar dentro de un virtual env.

![Image](https://github.com/users/centaurialpha/projects/7/assets/5894606/f4b9a353-1746-4761-95fd-ba7e438d7246)

```
python -m venv .venv
source .venv/bin/activate
(.venv) pip install -r requirements/front.txt
(.venv) pip install .
(.venv) tudu --help
```

## Tests
Para ejecutar los tests, los servicios deben estar levantados, luego:

```
make tests
```
