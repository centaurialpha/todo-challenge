from django.urls import path
from rest_framework.routers import DefaultRouter

from apps.core.api import views
from apps.tasks.api.views import TaskViewSet

default_router = DefaultRouter()
default_router.register(r"tasks", TaskViewSet, basename="tasks")

urlpatterns = [
    path("login", views.login),
    path("register", views.register),
] + default_router.urls
