from typing import ClassVar

from rest_framework import serializers

from apps.tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields: ClassVar = [
            "id",
            "title",
            "completed",
            "created_at",
        ]
