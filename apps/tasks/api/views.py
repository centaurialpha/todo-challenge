import logging
from typing import ClassVar

from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.tasks.api.serializers import TaskSerializer
from apps.tasks.models import Task

logger = logging.getLogger(__name__)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes: ClassVar = [IsAuthenticated]
    authentication_classes: ClassVar = [TokenAuthentication]

    def get_queryset(self):
        params = self.request.query_params
        queryset = super().get_queryset()
        if params:
            pattern = params["title"]
            queryset = queryset.filter(title__icontains=pattern)
        return queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        data = self.request.data["title"]
        logger.info("Creating task with data=%s, user=%s", data, self.request.user)
        serializer.save(user=self.request.user)

    @action(detail=False, methods=["GET"], url_path="not-completed")
    def not_completed_tasks(self, request):
        del request

        tasks = Task.objects.filter(completed=False)
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data)
