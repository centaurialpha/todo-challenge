import logging

logger = logging.getLogger("tasks")


class LogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        rmethod = request.method
        rpath = request.path
        logger.info("Executing %s in %s", rmethod, rpath)
        return response
