from django.contrib.auth.models import User
from django.db import models

from apps.core.models import TimestampedModel


class Task(TimestampedModel, models.Model):
    title = models.CharField(max_length=100)
    completed = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self) -> str:
        return repr(self)

    def __repr__(self) -> str:
        return f"{self.title}"
