#!/bin/bash

# Esperar a el socket de la DB
while ! nc -z db 5432; do
  echo "Esperando a la DB..."
  sleep 0.2
done

echo "Database started!"

# gunicorn config.wsgi:application --bind 0.0.0.0:8080 --workers 2 --threads 2  --log-level debug
python manage.py runserver 0.0.0.0:8000

exec "$@"
