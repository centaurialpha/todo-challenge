from dataclasses import dataclass
from datetime import datetime
from typing import Protocol

import humanize
import requests

BASE_API_URL: str = "http://localhost:4321/api/v1"


class Connection(Protocol):
    def get(self, endp: str) -> dict: ...

    def post(self, endp: str, data: str) -> dict: ...

    def delete(self, endp: str, pk: int) -> dict: ...

    def patch(self, endp: str, pk: int) -> dict: ...


class BaseClient:
    def __init__(self, base_url: str):
        self._base_url = base_url
        self._headers: dict[str, str] = {}

    def set_headers(self, key, value):
        self._headers[key] = value

    def _request(self, method, path, data=None) -> requests.Response:
        url = f"{self._base_url}/{path}"
        response = method(url, data=data, headers=self._headers)
        return response

    def get(self, path: str):
        response = self._request(method=requests.get, path=path)
        return response

    def post(self, path: str, data):
        response = self._request(method=requests.post, path=path, data=data)
        return response

    def delete(self, path: str):
        response = self._request(method=requests.delete, path=path)
        return response

    def patch(self, path: str):
        data = {"completed": True}
        response = self._request(method=requests.patch, path=path, data=data)
        return response


class TuduClient(BaseClient):
    def __init__(self, base_url: str) -> None:
        super().__init__(base_url)

    def _auth(self, username: str, password: str, path: str):
        user_data = {
            "username": username,
            "password": password,
        }
        response = self.post(path=path, data=user_data)
        if response.status_code in (200, 201):
            token = response.json()["token"]
            self.set_headers(key="Authorization", value=f"token {token}")
        return response

    def login(self, username: str, password: str):
        return self._auth(username, password, path="login")

    def register(self, username, password: str):
        return self._auth(username, password, path="register")

    def post_task(self, detail: str):
        response = self.post(path="tasks/", data={"title": detail})
        return response.json()

    def get_tasks(self):
        response = self.get(path="tasks/")
        return [Task(**t) for t in response.json()]

    def delete_task(self, task_id: int) -> int:
        response = self.delete(path=f"tasks/{task_id}/")
        return response.status_code

    def complete_task(self, task_id: int) -> int:
        response = self.patch(path=f"tasks/{task_id}/")
        return response.status_code

    def search_tasks(self, pattern: str):
        response = self.get(path=f"tasks/?title={pattern}")
        return [Task(**t) for t in response.json()]


@dataclass
class Task:
    id: int
    title: str
    completed: bool
    created_at: str

    @property
    def ctime(self) -> str:
        return humanize.naturaltime(datetime.fromisoformat(self.created_at))

    @property
    def completed_icon(self) -> str:
        return "[bold red]:x:" if not self.completed else "[green]:heavy_check_mark:"
