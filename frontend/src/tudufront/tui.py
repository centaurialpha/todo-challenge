import argparse
import sys
from pathlib import Path

from rich.console import Console
from rich.prompt import Prompt
from rich.table import Table

from tudufront.client import Task, TuduClient

console = Console()


_TOKEN_PATH = Path("/tmp/tudu_token")
_BASE_URL = "http://localhost:4321/api/v1"


def get_cli_parser(app) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="sub-command help")

    add_parser = subparsers.add_parser("add", help="Add new task")
    add_parser.add_argument("detail")
    add_parser.set_defaults(slot=app.add_task)

    list_parser = subparsers.add_parser("ls", help="List not completed tasks")
    list_parser.add_argument(
        "--ignore-completed", action="store_true", help="Dont show completed tasks"
    )
    list_parser.set_defaults(slot=app.list_tasks)

    delete_parser = subparsers.add_parser("delete", help="Delete task")
    delete_parser.add_argument("task_id", type=int)
    delete_parser.set_defaults(slot=app.delete_task)

    complete_parser = subparsers.add_parser("complete", help="Mark as completed")
    complete_parser.add_argument("task_id", type=int)
    complete_parser.set_defaults(slot=app.complete_task)

    search_parser = subparsers.add_parser("search", help="Search tasks")
    search_parser.add_argument("pattern")
    search_parser.set_defaults(slot=app.search_tasks)

    return parser


def token_required(func):
    def _wrapper(self, *args, **kwargs):
        if self._token:
            func(self, *args, **kwargs)

    return _wrapper


class Tudu:
    def __init__(self, client):
        self._client: TuduClient = client
        self._token, username = self._check_active_session()
        if self._token:
            self._user_logged(username)
        else:
            console.print("[cyan]No active session, please log in or register")
            self._token, username = self._init()
            if self._token:
                self._user_logged(username)

    def _user_logged(self, username: str):
        self._client.set_headers(key="Authorization", value=f"token {self._token}")
        console.rule(f"Logged as [blink bold magenta]{username}")

    def _check_active_session(self):
        if not _TOKEN_PATH.exists():
            return "", ""
        return _TOKEN_PATH.read_text().split()

    def _init(self):
        ans = Prompt.ask("[1] Login [2] Register?", choices=["1", "2"], default="1")
        if ans == "1":
            return self._auth(method=self._client.login)
        return self._auth(method=self._client.register)

    def _auth(self, method):
        username = console.input("Username: ")
        password = console.input("Password: ", password=True)
        # try:
        with console.status(f"Trying {method.__name__} with {username}..."):
            response = method(username, password)
            if response.status_code not in (200, 201):
                console.print(
                    f"[bold red]kernel panic, status_code={response.status_code}"
                )
                return "", ""

            token = response.json()["token"]
            session_text = f"{token} {username}"

            _TOKEN_PATH.write_text(session_text)
            return token, username

    def _print_tasks_table(self, tasks: list[Task], ignore_completed: bool = False):
        completed_tasks = [t for t in tasks if t.completed]
        not_completed_tasks = [t for t in tasks if not t.completed]

        table = Table(
            title="Tasks",
            caption=f"{len(completed_tasks)}/{len(tasks)}",
            caption_justify="right",
        )
        table.add_column("ID", header_style="bold bright_cyan", style="cyan")
        table.add_column("Created at", header_style="bold")
        table.add_column("Title", header_style="bold")
        table.add_column("Completed", header_style="bold")

        for task in not_completed_tasks:
            table.add_row(str(task.id), task.ctime, task.title, task.completed_icon)

        if not ignore_completed:
            for task in completed_tasks:
                table.add_row(
                    str(task.id),
                    task.ctime,
                    task.title,
                    task.completed_icon,
                    style="gray27",
                )

        console.print(table, justify="center")

    @token_required
    def list_tasks(self, ignore_completed: bool = False):
        with console.status("Getting tasks..."):
            all_tasks = self._client.get_tasks()
            if not all_tasks:
                console.print("[bold]No tasks :stuck_out_tongue:")
                return
        self._print_tasks_table(all_tasks, ignore_completed=ignore_completed)

    @token_required
    def add_task(self, detail: str):
        with console.status("Posting task..."):
            json_response = self._client.post_task(detail)
        task_id = json_response["id"]
        task_title = json_response["title"]
        console.print(f"[[green]{task_id}[/]] [white]'{task_title}' was added")

    @token_required
    def delete_task(self, task_id: int):
        self.list_tasks()
        ans = Prompt.ask(
            f"Remove task with id={task_id}?", choices=["yes", "no"], default="no"
        )
        if ans == "no":
            return
        with console.status("Deleting task..."):
            status_code = self._client.delete_task(task_id)
            if status_code != 204:
                console.print(f":warning: Task with id={task_id} does not exist")
            else:
                console.print("The task was deleted")

    @token_required
    def complete_task(self, task_id: int):
        with console.status("Marking task as completed..."):
            status_code = self._client.complete_task(task_id)
            if status_code != 200:
                console.print(f":warning: Task with id={task_id} does not exist")
            else:
                console.print("The task was marked as completed")

    @token_required
    def search_tasks(self, pattern: str):
        with console.status("Searching..."):
            tasks = self._client.search_tasks(pattern)
        self._print_tasks_table(tasks)


def main() -> None:
    client = TuduClient(base_url=_BASE_URL)
    app = Tudu(client)
    if len(sys.argv) == 1:
        app.list_tasks()
    else:
        args = get_cli_parser(app).parse_args()
        if hasattr(args, "slot"):
            kwargs = vars(args)
            func = kwargs.pop("slot")
            if callable(func):
                func(**kwargs)


if __name__ == "__main__":
    main()
