import pytest

from apps.tasks.models import Task
from django.contrib.auth.models import User


@pytest.fixture(autouse=True)
@pytest.mark.django_db
def logged_user(api_client):
    user = User.objects.create(username="test", password="test1234")
    api_client.force_authenticate(user)


@pytest.mark.django_db
@pytest.mark.parametrize("title", ["prueba", "pruebita 2", "esto es otra prueba"])
def test_add_task(api_client, title):
    task_data = {
        "title": title,
    }

    response = api_client.post("/api/v1/tasks/", data=task_data)
    assert response.status_code == 201

    task = Task.objects.get(pk=response.json()["id"])
    assert task.title == title


@pytest.mark.django_db
def test_complete_task(api_client):
    response = api_client.post("/api/v1/tasks/", data={"title": "ola"})
    task_id = response.json()["id"]
    response = api_client.patch(f"/api/v1/tasks/{task_id}/", data={"completed": True})
    assert response.status_code == 200


@pytest.mark.django_db
def test_delete_task(api_client):
    # Create tasks
    tasks_data = ["hola", "chau", "etonoecoca"]
    for data in tasks_data:
        api_client.post("/api/v1/tasks/", data={"title": data})

    # List tasks
    expected_tasks = len(tasks_data)
    response = api_client.get("/api/v1/tasks/")
    assert len(response.json()) == expected_tasks

    for _ in range(2):
        random_task = Task.objects.first()
        # Delete task
        api_client.delete(f"/api/v1/tasks/{random_task.pk}/")

    response = api_client.get("/api/v1/tasks/")
    assert len(response.json()) == 1


@pytest.mark.django_db
def test_search_task(api_client):
    tasks_data = ["HOLA", "ola", "chauuuu"]
    for data in tasks_data:
        api_client.post("/api/v1/tasks/", data={"title": data})

    pattern = "la"
    expected_tasks = 2

    response = api_client.get(f"/api/v1/tasks/?title={pattern}")
    assert len(response.json()) == expected_tasks
